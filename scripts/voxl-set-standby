#!/bin/bash
################################################################################
# Copyright 2021 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

CPU_CONTROL_PIPE=/run/mpa/cpu_monitor/control
COMMAND_SET_STANDBY_ON="set_standby_on"
COMMAND_SET_STANDBY_OFF="set_standby_off"
COMMAND_SET_STANDBY_AUTO="set_standby_auto"

usage () {
	echo "General Usage:"
	echo ""
	echo "voxl-set-standby on"
	echo "voxl-set-standby off"
	echo "voxl-set-standby auto"
	echo ""
	exit 0
}

if [ "$1" == "-h" ]; then
	usage
	exit 0
elif [ "$1" == "--help" ]; then
	usage
	exit 0
elif [ "$1" == "help" ]; then
	usage
	exit 0
fi


USER=$(whoami)
if [ "${USER}" != "root" ]; then
	echo "Run this script as the root user"
	exit 1
fi

if ! [ -p ${CPU_CONTROL_PIPE} ]; then
	echo "voxl-cpu-monitor is not running"
	exit 1;
fi


case "$1" in
	"on"|"ON")
		echo ${COMMAND_SET_STANDBY_ON} >> ${CPU_CONTROL_PIPE}
		;;
	"off"|"OFF")
		echo ${COMMAND_SET_STANDBY_OFF} >> ${CPU_CONTROL_PIPE}
		;;
	"auto"|"AUTO")
		echo ${COMMAND_SET_STANDBY_AUTO} >> ${CPU_CONTROL_PIPE}
		;;
	*)
		usage
		exit 1
		;;
esac


exit 0
