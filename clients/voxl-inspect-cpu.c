/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <getopt.h>
#include <unistd.h>	// for usleep()
#include <string.h>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>

#include "cpu_monitor_interface.h"
#include "common.h"



// this is the directory used by the voxl-cpu-monitor for named pipes
#define PIPE_DIR	(MODAL_PIPE_DEFAULT_BASE_DIR "cpu_monitor/")

#define CLIENT_NAME	"voxl-inspect-cpu"

#define TEMP_RED_THRESH 85
#define TEMP_YLW_THRESH 50
#define LOAD_RED_THRESH 80
#define LOAD_YLW_THRESH 40
#define MEM_RED_THRESH  3000
#define MEM_YLW_THRESH  2500


static void _print_usage(void)
{
	printf("\n\
Tool to print cpu monitor information to the screen for inspection\n\
\n\
Run this in debug mode to enable debug prints in the libmodal_pipe client code.\n\
You can also try the auto-reconnect mode which is useful for some projects but\n\
not all.\n\
\n\
-h, --help                  print this help message\n\
\n");
	return;
}

// called whenever the simple helper has data for us to process
static void _simple_cb(__attribute__((unused))int ch, char* raw_data, int bytes, __attribute__((unused)) void* context)
{
	int n_packets;
	cpu_stats_t *data_array = modal_cpu_validate_pipe_data(raw_data, bytes, &n_packets);
	if (data_array == NULL) return;

	// only use most recent packet
	cpu_stats_t data = data_array[n_packets-1];

	// prints header
	printf(CLEAR_TERMINAL RESET_FONT);
	printf(FONT_BOLD);
	printf("Name   Freq (MHz) Temp (C) Util (%%)\n");
	printf("-----------------------------------\n");
	printf(RESET_FONT);

	// prints cpu info
	for(int i=0; i < data.num_cpu; i++){
		printf("cpu%d %12.1f %s%8.1f %s%8.2f%s\n",
			i,
			(double)data.cpu_freq[i],
			GET_COLOR_GT(data.cpu_t[i], TEMP_RED_THRESH, TEMP_YLW_THRESH),
			(double)data.cpu_t[i],
			GET_COLOR_GT(data.cpu_load[i], LOAD_RED_THRESH, LOAD_YLW_THRESH),
			(double)data.cpu_load[i],
			RESET_FONT);
	}

	// print total cpu util %
	printf("Total             %s%8.1f %s%8.2f%s\n",
			GET_COLOR_GT(data.cpu_t_max, TEMP_RED_THRESH, TEMP_YLW_THRESH),
			(double)data.cpu_t_max,
			GET_COLOR_GT(data.total_cpu_load, LOAD_RED_THRESH, LOAD_YLW_THRESH),
			(double)data.total_cpu_load,
			RESET_FONT);
	printf("10s avg %s%27.2f%s\n",
			GET_COLOR_GT(data.cpu_load_10s, LOAD_RED_THRESH, LOAD_YLW_THRESH),
			(double)data.cpu_load_10s,
			RESET_FONT);

	// print small and big core totals
	printf("-----------------------------------\n");
	float avg_small = 0.0;
	float avg_big = 0.0;
	int half_cores = data.num_cpu/2;
	for(int i=0;i<half_cores; i++){
		avg_small+=data.cpu_load[i];
		avg_big  +=data.cpu_load[i+half_cores];
	}
	avg_small/=half_cores;
	avg_big  /=half_cores;

	printf("small cores only %s%18.2f%s\n",
			GET_COLOR_GT(data.cpu_load_10s, LOAD_RED_THRESH, LOAD_YLW_THRESH),
			(double)avg_small,
			RESET_FONT);
	printf("big cores only   %s%18.2f%s\n",
			GET_COLOR_GT(data.cpu_load_10s, LOAD_RED_THRESH, LOAD_YLW_THRESH),
			(double)avg_big,
			RESET_FONT);

	// print gpu data
	printf("-----------------------------------\n");
	printf("GPU  %12.1f %s%8.1f %s%8.2f%s\n" ,
		(double)data.gpu_freq,
		GET_COLOR_GT(data.gpu_t, TEMP_RED_THRESH, TEMP_YLW_THRESH),
		(double)data.gpu_t,
		GET_COLOR_GT(data.gpu_load, LOAD_RED_THRESH, LOAD_YLW_THRESH),
		(double)data.gpu_load,
		RESET_FONT);
	printf("GPU 10s avg                %s%8.2f%s\n" ,
		GET_COLOR_GT(data.gpu_load_10s, LOAD_RED_THRESH, LOAD_YLW_THRESH),
		(double)data.gpu_load_10s,
		RESET_FONT);

	// print memory data
	printf("-----------------------------------\n");
	printf("memory temp:      %s%5.1f C%s\n" ,
		GET_COLOR_GT(data.mem_t, TEMP_RED_THRESH, TEMP_YLW_THRESH),
		(double)data.mem_t,
		RESET_FONT);
	printf("memory used: %s%5d%s/%d MB\n",
		GET_COLOR_GT(data.mem_use_mb, MEM_RED_THRESH, MEM_YLW_THRESH),
		data.mem_use_mb,
		RESET_FONT,
		data.mem_total_mb);
	printf("-----------------------------------\n");
	printf("Flags\n");

	// CPU mode
	if(data.flags&CPU_STATS_FLAG_CPU_MODE_AUTO){
		printf("CPU freq scaling mode: auto\n");
	}
	else if(data.flags&CPU_STATS_FLAG_CPU_MODE_PERF){
		printf("CPU freq scaling mode: performance\n");
	}
	else if(data.flags&CPU_STATS_FLAG_CPU_MODE_POWERSAVE){
		printf("CPU freq scaling mode: powersave\n");
	}
	else if(data.flags&CPU_STATS_FLAG_CPU_MODE_CONSERVATIVE){
		printf("CPU freq scaling mode: conservative\n");
	}

	// Standby
	if(data.flags&CPU_STATS_FLAG_STANDBY_ACTIVE){
		printf("Standby Active\n");
	}
	else printf("Standby Not Active\n");

	// overload flags
	if(data.flags&CPU_STATS_FLAG_CPU_OVERLOAD){
		printf(COLOR_RED "CPU OVERLOAD WARNING" RESET_FONT "\n");
	}
	if(data.flags&CPU_STATS_FLAG_CPU_OVERHEAT){
		printf(COLOR_RED "CPU OVERHEAT WARNING" RESET_FONT "\n");
	}
	printf("-----------------------------------\n");
	fflush(stdout);
	return;
}


// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, CLEAR_TERMINAL FONT_BLINK "voxl-inspect-cpu: Server Disconnected" RESET_FONT);
	return;
}


// not many command line arguments
static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"help",			no_argument,		0,	'h'},
		{0, 0, 0, 0}
	};
	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "h", long_options, &option_index);
		if(c == -1) break; // Detect the end of the options.
		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;
		case 'h':
			_print_usage();
			return -1;
		default:
			_print_usage();
			return -1;
		}
	}
	return 0;
}


int main(int argc, char* argv[])
{
	int ret;

	// check for options
	if(_parse_opts(argc, argv)) return -1;

	// set some basic signal handling for safe shutdown.
	// quitting without cleanup up the pipe can result in the pipe staying
	// open and overflowing, so always cleanup properly!!!
	enable_signal_handler();
	main_running = 1;
	printf("attempting to connect to cpu monitor\n");

	// assign callabcks for data, connection, and disconnect. the "NULL" arg
	// here can be an optional void* context pointer passed back to the callbacks
	pipe_client_set_simple_helper_cb(0, _simple_cb, NULL);
	pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);

	// for this test we will use the simple helper
	int flags = EN_PIPE_CLIENT_SIMPLE_HELPER;

	// init connection to server. In auto-reconnect mode this will "succeed"
	// even if the server is offline, but it will connect later on automatically
	ret = pipe_client_open(0, PIPE_DIR, CLIENT_NAME, flags, CPU_STATS_RECOMMENDED_READ_BUF_SIZE);

	// check for success
	if(ret){
		fprintf(stderr, "ERROR opening channel:\n");
		pipe_print_error(ret);
		return -1;
	}

	// keep going until signal handler sets the main_running flag to 0
	while(main_running){
		usleep(500000);
	}

	// all done, signal pipe read threads to stop
	printf("closing\n");
	fflush(stdout);
	pipe_client_close_all();

	return 0;
}
