/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with hardware devices provided by
 *    ModalAI® Inc. Reverse engineering or copying this code for use on hardware 
 *    not manufactured by ModalAI is not permitted.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <pthread.h>
#include <getopt.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>

#include <c_library_v2/common/mavlink.h>
#include <modal_start_stop.h>
#include <modal_pipe_server.h>
#include <modal_pipe_client.h>

#include "common.h"
#include "cpu_monitor_interface.h"
#include "config_file.h"

#define PROCESS_NAME	"voxl-cpu-monitor" // name of pid file

#define SERVER_PIPE_CH	0
#define PIPE_NAME		"cpu_monitor"
#define PIPE_LOCATION	(MODAL_PIPE_DEFAULT_BASE_DIR PIPE_NAME "/")

// slow data rate pipe, only need heartbeats
#define MAV_PIPE_NAME	"mavlink_to_gcs"
#define MAV_PIPE_CH		0


#define STR_CPU_PERF			"performance"
#define STR_CPU_AUTO_APQ8096	"interactive"
#define STR_CPU_AUTO_QRB5165	"schedutil"
#define STR_CPU_POWERSAVE		"powersave"
#define STR_CPU_CONSERVATIVE	"conservative"

#define STR_GPU_AUTO "msm-adreno-tz"

// Location for system temperatures
// /sys/devices/virtual/thermal/thermal_zone<x>/temp
#define SYSTEM_THERMAL_DIRECTORY				"/sys/devices/virtual/thermal/thermal_zone"
#define SYSTEM_CURRENT_TEMPERATURE				"/temp"
#define SYSTEM_GPU_CURRENT_TEMPERATURE			"/sys/devices/virtual/thermal/thermal_zone15/temp"

// Location for CPU frequency information
#define SYSTEM_SPECIFIC_CPU_DIRECTORY_PREFIX	"/sys/devices/system/cpu/cpu"
#define SYSTEM_CPU_MAXIMUM_FREQUENCY			"/cpufreq/cpuinfo_max_freq"
#define SYSTEM_CPU_SCALING_FREQUENCY			"/cpufreq/scaling_cur_freq"
#define SYSTEM_CPU_GOVERNOR_PATH				"/cpufreq/scaling_governor"

#define CPU_MODE_AUTO			0
#define CPU_MODE_PERF			1
#define CPU_MODE_POWERSAVE		2
#define CPU_MODE_CONSERVATIVE	3
#define GPU_MODE_PERF	0
#define GPU_MODE_AUTO	1

// commands for extracting various cpu date
#define SYSTEM_CPU_COUNT						"ls /sys/devices/system/cpu | grep \"cpu[[:digit:]]\" | wc -l"
#define SYSTEM_CPU_FREQUENCY_COUNTERS			"/cpufreq/stats/time_in_state"

// location for gpu information
#define SYSTEM_GPU_FREQUENCY_COUNTERS			"/sys/class/kgsl/kgsl-3d0/gpu_clock_stats"
#define SYSTEM_GPU_BUSY_COUNTER					"/sys/class/kgsl/kgsl-3d0/gpubusy"
#define SYSTEM_GPU_AVAILABLE_FREQUENCIES		"/sys/class/kgsl/kgsl-3d0/gpu_available_frequencies"
#define SYSTEM_GPU_GOVERNOR_PATH				"/sys/class/kgsl/kgsl-3d0/devfreq/governor"
#define SYSTEM_GPU_CLOCK						"/sys/class/kgsl/kgsl-3d0/gpuclk"
#define PWM_DUTY_PATH							"/sys/class/pwm/pwmchip0/pwm0/duty_cycle"
#define SYSTEM_LED_PATH_RED						"/sys/class/leds/red/brightness"
#define SYSTEM_LED_PATH_GREEN					"/sys/class/leds/green/brightness"
#define SYSTEM_LED_PATH_BLUE					"/sys/class/leds/blue/brightness"
#define MEM_USED								"/proc/meminfo"

#define AUTO_FAN_BOUND_UPPER	80
#define AUTO_FAN_BOUND_LOWER	30
#define GPU_CLOCK_VALUES		7
#define CPU_STATS				9
#define SAMPLE_RATE_HZ			1

#define CHIP_APQ8096 0
#define CHIP_QRB5165 1

#define STANDBY_STATE_OFF		0 // normal CPU mode
#define STANDBY_STATE_ON		1 // standby mode
#define STANDBY_STATE_IGNORE	2 // user overwrote the cpu scaler via command, ignore all standby logic

#define FAN_MODE_AUTO	0
#define FAN_MODE_OFF	1
#define FAN_MODE_MAX		2
#define FAN_MODE_SLOW	3


// local vars
static int en_debug;
static float current_cpu_temp = 0; // used for auto fan speed control
static int cpu_mode = CPU_MODE_AUTO;
static int gpu_mode = GPU_MODE_AUTO;
static int chip = CHIP_QRB5165;
static int is_autopilot_armed = 1; // assume we are armed until told otherwise

static int current_standby_state = 0; // OFF 0 or ON (standby active) 1

// collects cpu tick information for calculating cpu util %
static int previous_cpu_ticks[CPU_MON_MAX_CPU][CPU_STATS];
static int cpu_ticks[CPU_MON_MAX_CPU][CPU_STATS];
static char cpu_governor_dir[CPU_MON_MAX_CPU][100] = {""};

// collects cpu frequency information for calculating cpu util %
static float previous_frequency_counts[CPU_MON_MAX_CPU][30];
static float frequency_counts[CPU_MON_MAX_CPU][30];

// stores gpu frequency information for calculating gpu util %
static float gpu_frequencies[GPU_CLOCK_VALUES] = {0};

// Multiplier for long term CPU utilization
#define FILTER_SAMPLES		10
static float prev_cpu_load[FILTER_SAMPLES];
static float prev_gpu_load[FILTER_SAMPLES];

//Variables for LED control
static unsigned char led_flashing = 0;
static int led_r,led_g,led_b;
static unsigned char led_cur_on = 0;

// printed if some invalid argument was given
static void _print_usage(void)
{
	printf("\n\
voxl-cpu-monitor usually runs as a systemd background service. However, for debug\n\
purposes it can be started from the command line manually.\n\
When started from the command line, the background service will\n\
stop automatically so you don't have to stop it manually.\n\
\n\
-c, --load_config_only      Load the config file and then exit right away.\n\
                              This also adds new defaults if necessary. This is used\n\
                              by voxl-configure-cpu-monitor to make sure the config file\n\
                              is up to date without actually starting this service.\n\
-d, --debug                 enabel debug prints\b\
-h, --help                  print this help message\n\
\n");
	return;
}


static void _autopilot_helper_cb(__attribute__((unused))int ch, char* data, int bytes, __attribute__((unused)) void* context)
{
	// validate that the data makes sense
	int n_packets;
	mavlink_message_t* msg_array = pipe_validate_mavlink_message_t(data, bytes, &n_packets);
	if(msg_array == NULL){
		return;
	}

	for(int i=0; i<n_packets; i++){
		mavlink_message_t* msg = &msg_array[i];

		if(msg->msgid != MAVLINK_MSG_ID_HEARTBEAT){
			continue;
		}

		uint8_t heartbeat_base_mode = mavlink_msg_heartbeat_get_base_mode(msg);
		if(heartbeat_base_mode & MAV_MODE_FLAG_SAFETY_ARMED){
			if(en_debug && !is_autopilot_armed) printf("AUTOPILOT ARMED\n");
			is_autopilot_armed = 1;
		}
		else{
			if(en_debug && is_autopilot_armed) printf("AUTOPILOT DISARMED\n");
			is_autopilot_armed = 0;
		}
	}

	return;
}

static void _connect_to_autopilot(void)
{
	static int has_opened_connection_to_autopilot = 0;

	if(has_opened_connection_to_autopilot) return;

	pipe_client_set_simple_helper_cb(MAV_PIPE_CH, _autopilot_helper_cb, NULL);
	pipe_client_open(MAV_PIPE_CH, MAV_PIPE_NAME, PROCESS_NAME, \
					EN_PIPE_CLIENT_SIMPLE_HELPER | EN_PIPE_CLIENT_AUTO_RECONNECT, \
											sizeof(mavlink_message_t));
	has_opened_connection_to_autopilot = 1;
	return;
}

// finds out how many cpus there are
static uint32_t _get_cpu_count()
{
	uint32_t cpu_count;
	FILE *cpu_fp;

	cpu_fp = popen(SYSTEM_CPU_COUNT, "r");

	if(cpu_fp == NULL){
		return -1;
	}

	fscanf(cpu_fp, "%d", &cpu_count);

	pclose(cpu_fp);
	return cpu_count;
}

// sets a duty cycle between 0 and 40000
static void _set_fan_duty(int i)
{
	// remember if we've exported the driver
	static int pwm_not_enabled = 0;
	static int is_exported = 0;
	char str[8];
	int fd;

	// pwm only works on qrb
	if(chip != CHIP_APQ8096) return;

	// quit if pwm failed to export
	if(pwm_not_enabled) return;

	// make sure value is bounded
	if(i<0){
		i=0;
	}
	else if(i>40000){
		i=40000;
	}

	// check that the driver is exported
	if(is_exported==0){
		if(access(PWM_DUTY_PATH, F_OK)!=0){
			// not exported yet! try to export
			system("echo 0 > /sys/class/pwm/pwmchip0/export");
			usleep(10000);
			if(access(PWM_DUTY_PATH, F_OK)!=0){
				fprintf(stderr, "ERROR, failed to export fan pwm channel\n");
				fprintf(stderr, "pwm fan not yet supported on QRB5165\n");
				pwm_not_enabled = 1;
				return;
			}
			// great! driver exported. Make sure period and enable are set
			system("echo 40000 > /sys/class/pwm/pwmchip0/pwm0/period");
			system("echo 1 > /sys/class/pwm/pwmchip0/pwm0/enable");
			is_exported = 1;
		}
		else{
			// can open period path, so export is good!
			is_exported = 1;
		}
	}

	// normal operation, open duty cycle for writing
	fd = open(PWM_DUTY_PATH, O_WRONLY);
	if(fd<0){
		perror("ERROR failed to open PWM period for writing");
		return;
	}
	int n_char = sprintf(str, "%d",i);
	int ret = write(fd, str, n_char+1);
	if(ret!=n_char+1){
		perror("failed to write to PWM period");
	}

	close(fd);
	return;
}

static void _set_fan(int mode)
{
	int duty;

	switch(mode){

	case FAN_MODE_AUTO: // auto
		// just turn off the fan at the low end, it doesn't like low duty cycles
		duty = 40000 * ((current_cpu_temp-AUTO_FAN_BOUND_LOWER)/(AUTO_FAN_BOUND_UPPER-AUTO_FAN_BOUND_LOWER));

		// upper and lower bound, fan doesn't like really low duty cycle
		if(duty>40000) duty = 40000;
		else if(duty<10000) duty = 0;

		if(en_debug){
			printf("auto_fan max_t: %4.1f duty: %d\n", (double)current_cpu_temp, duty);
		}

		break;

	case FAN_MODE_OFF:
		duty = 0;
		break;

	case FAN_MODE_MAX:
		duty = 40000;
		break;

	case FAN_MODE_SLOW:
		duty = 20000;
		break;

	default:
		fprintf(stderr, "ERROR unknown fan mode: %d\n", mode);
		return;
	}

	// update extern variable from config file
	fan_mode = mode;
	_set_fan_duty(duty);

	return;
}

static void _set_cpu_mode(int new_mode)
{
	if(en_debug){
		printf("setting cpu mode to %d\n", new_mode);
	}

	for(uint i = 0; i < _get_cpu_count(); i++){
		int ret, fd;
		fd = open(cpu_governor_dir[i], O_WRONLY);
		if(fd==-1){
			perror("ERROR in _set_cpu_mode opening file");
			return;
		}


		switch(new_mode){
			case CPU_MODE_AUTO: // default mode

				if(chip == CHIP_APQ8096){
					ret=write(fd, STR_CPU_AUTO_APQ8096, 11);
				}else if (chip == CHIP_QRB5165){
					ret=write(fd, STR_CPU_AUTO_QRB5165, 9);
				}else{
					fprintf(stderr,"ERROR in %s, invalid chip code\n", __FUNCTION__);
				}
				break;
			case CPU_MODE_PERF:
				ret=write(fd, STR_CPU_PERF, 11);
				break;
			case CPU_MODE_POWERSAVE:
				ret=write(fd, STR_CPU_POWERSAVE, 9);
				break;
			case CPU_MODE_CONSERVATIVE:
				ret=write(fd, STR_CPU_CONSERVATIVE, 12);
				break;
			default:
				fprintf(stderr,"ERROR in %s, invalid cpu mode\n", __FUNCTION__);
		}

		if(ret==-1){
			perror("ERROR setting cpu mode");
		}
		close(fd);
	}
	cpu_mode = new_mode;
	return;
}


static void _get_cpu_mode()
{
	char buf[20];

	int fd = open(cpu_governor_dir[0], O_RDONLY);
	if(fd<0){
		perror("ERROR failed to open cpu mode for reading");
		return;		
	}

	int ret = read(fd, buf, sizeof(buf));
	if(ret<1){
		perror("ERROR failed to read cpu mode");
		close(fd);
		return;
	}
	buf[ret-1] = 0;

	if(strcmp(buf, STR_CPU_PERF)==0){
		cpu_mode = CPU_MODE_PERF;
	} else if(strcmp(buf, STR_CPU_POWERSAVE)==0){
		cpu_mode = CPU_MODE_POWERSAVE;
	} else if(strcmp(buf, STR_CPU_CONSERVATIVE)==0){
		cpu_mode = CPU_MODE_CONSERVATIVE;
	} else {
		cpu_mode = CPU_MODE_AUTO;
	}

	close(fd);
}


static void _get_gpu_mode()
{
	char buf[20];
	
	int fd = open(SYSTEM_GPU_GOVERNOR_PATH, O_RDONLY);
	if(fd<0){
		perror("ERROR failed to open gpu mode for reading");
		return;		
	}

	int ret = read(fd, buf, sizeof(buf));
	if(ret<1){
		perror("ERROR failed to read gpu mode");
		close(fd);
		return;
	}
	buf[strlen(STR_CPU_PERF)] = 0;

	if(strcmp(buf, STR_CPU_PERF)){
		gpu_mode = GPU_MODE_AUTO;
	} else {
		gpu_mode = GPU_MODE_PERF;
	}

	close(fd);
}

static void _update_led()
{
	int r,g,b;

	if(led_cur_on){
		r = 0;
		g = 0;
		b = 0;
	} else {
		r = led_r;
		g = led_g;
		b = led_b;
	}

	char command[256];

	sprintf(command, 
		"echo %d > "SYSTEM_LED_PATH_RED" && "\
		"echo %d > "SYSTEM_LED_PATH_GREEN" && "\
		"echo %d > "SYSTEM_LED_PATH_BLUE,
		r, g, b);
	system(command);


	//If flashing invert led_cur_on
	led_cur_on = !led_flashing || !led_cur_on;
}

static void _set_led(int r, int g, int b, unsigned char flashing)
{
	led_r = TRUNCATE(r, 0, 255);
	led_g = TRUNCATE(g, 0, 255);
	led_b = TRUNCATE(b, 0, 255);
	led_flashing = flashing;
	led_cur_on = 0;

	_update_led();
}

#define CHECK_LED_VAL() if(val==NULL){printf("Error parsing custom led string\n");return;}
//input string here should follow the pattern: 
// set_led_mode_color r g b flash
//flash is optional
static void _set_led_from_string(char * string)
{
	unsigned char flashing = strstr(string, "flash") != NULL;
	char * val = strtok(string, " ");
	CHECK_LED_VAL();
	val = strtok(NULL, " ");
	CHECK_LED_VAL();
	int r = atoi(val);
	val = strtok(NULL, " ");
	CHECK_LED_VAL();
	int g = atoi(val);
	val = strtok(NULL, " ");
	CHECK_LED_VAL();
	int b = atoi(val);

	printf("Custom color: R:%d, G:%d, B:%d, Flashing: %s\n", r,g,b, flashing ? "yes" : "no");

	_set_led(r,g,b,flashing);
}

static void _control_pipe_handler(__attribute__((unused)) int ch, char* string, int bytes, __attribute__((unused)) void* context)
{
	// remove the training newline from echo
	if(bytes>1 && string[bytes-1]=='\n'){
		string[bytes-1]=0;
	}

	// check each command
	if(strcmp(string, COMMAND_SET_CPU_MODE_AUTO)==0){
		printf("Got Command to set CPU to auto mode\n");
		en_auto_standby = 0;
		current_standby_state = STANDBY_STATE_IGNORE;
		_set_cpu_mode(CPU_MODE_AUTO);
		return;
	}
	if(strcmp(string, COMMAND_SET_CPU_MODE_PERF)==0){
		printf("Got Command to set CPU to performance mode\n");
		en_auto_standby = 0;
		current_standby_state = STANDBY_STATE_IGNORE;
		_set_cpu_mode(CPU_MODE_PERF);
		return;
	}
	if(strcmp(string, COMMAND_SET_CPU_MODE_POWERSAVE)==0){
		printf("Got Command to set CPU to powersave mode\n");
		en_auto_standby = 0;
		current_standby_state = STANDBY_STATE_IGNORE;
		_set_cpu_mode(CPU_MODE_POWERSAVE);
		return;
	}
	if(strcmp(string, COMMAND_SET_CPU_MODE_CONSERVATIVE)==0){
		printf("Got Command to set CPU to conservative mode\n");
		en_auto_standby = 0;
		current_standby_state = STANDBY_STATE_IGNORE;
		_set_cpu_mode(CPU_MODE_CONSERVATIVE);
		return;
	}
	if(strcmp(string, COMMAND_SET_FAN_MODE_OFF)==0){
		printf("Got Command to set fan mode to OFF\n");
		_set_fan(FAN_MODE_OFF);
		return;
	}
	if(strcmp(string, COMMAND_SET_FAN_MODE_SLOW)==0){
		printf("Got Command to set fan duty to 20000\n");
		_set_fan(FAN_MODE_SLOW);
		return;
	}
	if(strcmp(string, COMMAND_SET_FAN_MODE_MAX)==0){
		_set_fan(FAN_MODE_MAX);
		return;
	}
	if(strcmp(string, COMMAND_SET_FAN_MODE_AUTO)==0){
		_set_fan(FAN_MODE_AUTO);
		return;
	}
	if(strcmp(string, COMMAND_SET_LED_MODE_OFF)==0){
		printf("Got Command to set led to off\n");
		_set_led(0,0,0,0);
		return;
	}
	if(strcmp(string, COMMAND_SET_LED_MODE_ON)==0){
		printf("Got Command to set led to on\n");
		_set_led(255,255,255,0);
		return;
	}
	if(strncmp(string, COMMAND_SET_LED_MODE_COLOR, strlen(COMMAND_SET_LED_MODE_COLOR))==0){
		printf("Got Command to set led to custom\n");
		_set_led_from_string(string);
		return;
	}
	if(strcmp(string, COMMAND_SET_STANDBY_ON)==0){
		printf("Got Command to set standby mode on\n");
		en_auto_standby = 0;
		current_standby_state = STANDBY_STATE_ON;
		return;
	}
	if(strcmp(string, COMMAND_SET_STANDBY_OFF)==0){
		printf("Got Command to set standby mode off\n");
		en_auto_standby = 0;
		current_standby_state = STANDBY_STATE_OFF;
		return;
	}
	if(strcmp(string, COMMAND_SET_STANDBY_AUTO)==0){
		printf("Got Command to set standby mode to auto\n");
		en_auto_standby = 1;
		current_standby_state = STANDBY_STATE_OFF;
		return;
	}


	printf("WARNING: received unknown command through the control pipe!\n");
	printf("got %d bytes: %s\n", bytes, string);
	return;
}


static void _connect_handler(int ch, int client_id, char* string, __attribute__((unused)) void* context)
{
	printf("client \"%s\" connected to channel %d  with client id %d\n", string, ch, client_id);
	return;
}


static void _disconnect_handler(int ch, int client_id, char* name, __attribute__((unused)) void* context)
{
	printf("client \"%s\" with id %d has disconnected from channel %d\n", name, client_id, ch);
	return;
}

static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"load_config_only",	no_argument,	0,	'c'},
		{"debug",				no_argument,	0,	'd'},
		{"help",				no_argument,	0,	'h'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "cdh", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'c':
			config_file_read();
			exit(0);
			break;
		case 'd':
			en_debug = 1;
			break;

		case 'h':
			_print_usage();
			return -1;

		default:
			_print_usage();
			return -1;
		}
	}

	return 0;
}


// grabs the frequency of an individual cpu
static float _get_cpu_current_frequency(char cpu_cur_freq_dir[])
{
	float cpu_cur_freq; // frequency value
	char buf[20];
	int fd, ret;

	fd = open(cpu_cur_freq_dir, O_RDONLY);
	if(fd<0){
		// CPU is likely off
		//perror("ERROR failed to open cpu scaling frequency scaling for reading");
		return 0.0;
	}

	ret = read(fd, buf, sizeof(buf));
	if(ret<1){
		perror("ERROR failed to read cpu frequency scaling");
		close(fd);
		return 0;
	}
	sscanf(buf, "%f", &cpu_cur_freq);

	close(fd);
	return cpu_cur_freq/1000; // convert to correct units
}


// grabs temperature of x device. value is obtained from the thermal directory
static float _get_x_current_temp(char thermal_zone_dir[], float thermal_conversion)
{
	float cpu_cur_temp; // current temperature of x device
	
	char buf[15];
	int fd, ret;

	fd = open(thermal_zone_dir, O_RDONLY);
	if(fd<0){
		perror("ERROR failed to open temp for reading");
		return 0;
	}

	ret = read(fd, buf, sizeof(buf));
	if(ret<1){
		perror("ERROR failed to read temp");
		close(fd);
		return 0;
	}

	sscanf(buf, "%f", &cpu_cur_temp);
	cpu_cur_temp *= thermal_conversion; // converts to appropriate units

	close(fd);
	return cpu_cur_temp;
}

// calculates weighted frequency of an individual cpu
static float _get_cpu_weighted_frequency(int cpu_val, char cpu_weighted_freq_dir[])
{	
	int idx, n;
	int offset = 0;
	int freq_list_size = 0; // size of the frequency list obtained during runtime
	float frequencies[50]; // frequency values to be stored
	char buf[400]; // large enough to store entire file with max values
	int fd, ret; // opens files to retrieve frequency values

	fd = open(cpu_weighted_freq_dir, O_RDONLY);
	if(fd<0){
		// core is likely off
		//perror("ERROR failed to open cpu weighted frequency for reading");
		return 0.0;
	}	
	ret = read(fd, buf, sizeof(buf));
	if(ret<1){
		perror("ERROR failed to read cpu weighted frequency");
		close(fd);
		return 0;
	}
	while (sscanf(buf + offset, "%f %f%n", &frequencies[freq_list_size], &frequency_counts[cpu_val][freq_list_size], &n) == 2) {
		offset += n;
		freq_list_size++;
	}

	float weighted_frequencies = 0;
	float delta_counts[freq_list_size];
	float total_frequency_count = 0;
	float tmp_val;

	// calculates change(delta) for each frequency count
	for(idx = 0; idx < freq_list_size; idx++){
		// delta = current frequency count - previous frequency count
		delta_counts[idx] = frequency_counts[cpu_val][idx] - previous_frequency_counts[cpu_val][idx];
		total_frequency_count += delta_counts[idx]; // sum of all changes
	}

	if (total_frequency_count > 0){ // checks if there is a change
		for (idx = 0; idx < freq_list_size; idx++){
			// to calculate the weighted frequencies, for each delta(change) we divide by the total frequency count and store it in a temp value,
			// multiply each temp value by its frequency, and sum all the values together
			tmp_val = delta_counts[idx] / total_frequency_count;
			tmp_val *= frequencies[idx];
			weighted_frequencies += tmp_val;
		}
	}

	for (idx = 0; idx < freq_list_size; idx++){ // update previous frequency values
		previous_frequency_counts[cpu_val][idx] = frequency_counts[cpu_val][idx];
	}

	close(fd);
	return weighted_frequencies;

}


// grabs max cpu frequncy
static float _get_cpu_max_freq(char cpu_max_freq_dir[])
{
	float cpu_max_freq; // max frequency

	char buf[15];
	int fd, ret;

	fd = open(cpu_max_freq_dir , O_RDONLY);
	if(fd<0){
		// core is likely off
		//perror("ERROR failed to open cpu max frequency for reading");
		return 0.0;
	}	
	
	ret = read(fd, buf, sizeof(buf));
	if(ret<1){
		perror("ERROR failed to read cpu max frequency");
		close(fd);
		return 0;
	}
	sscanf(buf, "%f", &cpu_max_freq);
	
	close(fd);
	return cpu_max_freq;
}
#define PROC_STAT_DIRECTORY "/proc/stat"

// gets cpu load of an individual cpu
static float _get_cpu_load(int cpu_val, float cpu_max_freq, float weighted_frequency)
{
	int idx, n;
	int offset = 0;

	int total_ticks = 0;
	int idle_ticks = 0;
	int prev_total_ticks = 0;
	int prev_idle_ticks = 0;
	float total_period, idle_period;

	float cpu_load = 0;
	FILE *cpu_fp;

	char ps_line[100];
	int cpu_line_location = cpu_val + 1; // line of each cpu is one above their cpu_val index in /proc/stat

	cpu_fp = fopen(PROC_STAT_DIRECTORY, "r");
	if(cpu_fp == NULL){
		perror("ERROR failed to open /proc/stat for reading");
		return 0;
	}

	for (idx = 0; idx <= cpu_line_location; idx++){
		fgets(ps_line, sizeof(ps_line), cpu_fp); // iterates to correct line and stores
		if (ferror(cpu_fp)){
			perror("ERROR failed to read from /proc/stat");
			fclose(cpu_fp);
			return 0;
		}
	}
	
	sscanf(ps_line, "%*s %n", &offset);
	for (idx = 0; idx < CPU_STATS; idx++){ // grabs 9 values from proc/stat/
		sscanf(ps_line + offset, "%d %n", &cpu_ticks[cpu_val][idx], &n);

		total_ticks += cpu_ticks[cpu_val][idx]; // total ticks is the sum of all values
		prev_total_ticks += previous_cpu_ticks[cpu_val][idx]; // retrieving from previous, this value is global

		if (idx == 3){ // third tick stores idle time
			idle_ticks = cpu_ticks[cpu_val][idx];
			prev_idle_ticks = previous_cpu_ticks[cpu_val][idx];
		}
		offset += n;
	}

	if (previous_cpu_ticks[cpu_val][0] != 0 || total_ticks != 0){ // check
		//To calculate cpu load you need to calculate idle period and total_period
		//Subtract total period by idle period and divide by the total period ((idle_period-total_period)/total_period)
		//Multiply this value by 100
		//Multiply this value by (weighted_frequency / cpu_max_freq), these values are obtained in previous functions

		idle_period = idle_ticks - prev_idle_ticks;
		total_period = total_ticks - prev_total_ticks;

		cpu_load = total_period - idle_period;
		cpu_load = cpu_load / total_period;
		cpu_load *= 100;

		cpu_load *= (weighted_frequency / cpu_max_freq);

		if(en_debug){
			printf("cpu: %d idle:%6.0f total:%6.0f maxf:%8.0f cpu_load:%6.2f\n",\
			cpu_val, (double)idle_period, (double)total_period, \
			(double)cpu_max_freq, (double)cpu_load);
		}
	}

	for (idx = 0; idx < 7; idx++){ // update previous values
		previous_cpu_ticks[cpu_val][idx] = cpu_ticks[cpu_val][idx];
	}
					
	fclose(cpu_fp);
	return cpu_load;
}

// sets global gpu_frequency array
static void _get_gpu_frequencies()
{
	int idx;
	char buf[10];
	int fd, ret;;

	fd = open(SYSTEM_GPU_AVAILABLE_FREQUENCIES, O_RDONLY);
	if(fd<0){
		perror("ERROR failed to open gpu available frequencies for reading");
		return;
	}
	for (idx = 0; idx < GPU_CLOCK_VALUES; idx++){
		ret = read(fd, buf, sizeof(buf));
		if(ret<1){
			perror("ERROR failed to read gpu available frequencies");
			close(fd);
			return;
		}
		sscanf(buf, "%f", &gpu_frequencies[idx]); // sets global gpu frequencies
	}

	 close(fd);
}


// gets gpu busy value
static float _get_gpu_busy()
{
	fflush(stdout);
	float gpu_busy[2]; // stores busy values
	float gpu_busy_ret = 0;
	
	char buf[15];
	int fd, ret;

	fd = open(SYSTEM_GPU_BUSY_COUNTER, O_RDONLY);
	if(fd<0){
		perror("ERROR failed to open gpu busy counter for reading");
		return 0;
	}

	ret = read(fd, buf, sizeof(buf));
	if(ret<1){
		perror("ERROR failed to read gpu busy counter");
		close(fd);
		return 0;
	}
	sscanf(buf, "%f %f", &gpu_busy[0], &gpu_busy[1]);

	if (gpu_busy[1] == 0){ // check if gpu_busy[1] is 0 to avoid divide by 0 errors
		close(fd);
		return 0;
	}
	gpu_busy_ret = (gpu_busy[0] / gpu_busy[1])*(float)100;

	close(fd);
	return gpu_busy_ret;
}

// calculates gpu load %
static void _get_gpu_load(cpu_stats_t* stats)
{
	int idx;
	float gpu_utilization = _get_gpu_busy();
	float gpu_clk;

	char buf[75];
	int fd_clk, ret;

	// reading gpu clock value
	fd_clk = open(SYSTEM_GPU_CLOCK, O_RDONLY);
	if(fd_clk<0){
		perror("ERROR failed to open gpu clock for reading");
		return;
	}
	ret = read(fd_clk, buf, sizeof(buf));
	close(fd_clk);
	if(ret<1){
		perror("ERROR failed to read gpu clock");
		close(fd_clk);
		return;
	}
	sscanf(buf, "%f", &gpu_clk); // reads gpu clock value
	gpu_clk /= 1000000.0l; // changes clock value to appropriate format

	// shift previous record of load
	for(int i=FILTER_SAMPLES-1; i>0; i--){
		prev_gpu_load[i] = prev_gpu_load[i - 1];
	}
	prev_gpu_load[0] = gpu_utilization;

	// find average
	float gpu_utilization_avg = 0;
	for(idx=0; idx<FILTER_SAMPLES; idx++){
		gpu_utilization_avg += prev_gpu_load[idx];
	}
	gpu_utilization_avg /= FILTER_SAMPLES;

	// write out data
	stats->gpu_freq		= gpu_clk;
	stats->gpu_load		= gpu_utilization;
	stats->gpu_load_10s = gpu_utilization_avg;

	return;
}

// grabs memory used
static void _get_mem_used(cpu_stats_t* data)
{
	int tot_mem, available_mem, used_mem;
	char buf[128]; // length of entire line
	int fd, ret;
	static int quit = 0;

	if(quit) return;

	fd = open(MEM_USED , O_RDONLY);
	if(fd<0){
		perror("ERROR failed to open meminfo for reading");
		return;
	}

	ret = read(fd, buf, sizeof(buf));
	if(ret<1){
		perror("ERROR failed to read meminfo");
		close(fd);
		return;
	}
	
	ret = sscanf(buf, "MemTotal: %d %*s\n%*s %*d %*s\n%*s %d", &tot_mem, &available_mem);

	if(ret!=2){
		fprintf(stderr, "ERROR scanning /proc/meminfo\n");
		quit = 1;
		return;
	}

	used_mem = tot_mem - available_mem;
	data->mem_use_mb = used_mem/1024;
	data->mem_total_mb = tot_mem/1024;
	close(fd);
	return;
}

// calculates total cpu load averaged between al cpu's
static void _get_total_cpu_load(cpu_stats_t* data)
{
	int i;
	float total_cpu_load = 0.0f;

	for(i=0; i<data->num_cpu; i++){
		total_cpu_load += data->cpu_load[i]; // sums up all cpu loads
	}
	total_cpu_load /= (float)data->num_cpu; // averages all loads by number of cpu's
	data->total_cpu_load = total_cpu_load;


	// shift previous record of load
	for(int i=FILTER_SAMPLES-1; i>0; i--){
		prev_cpu_load[i] = prev_cpu_load[i - 1];
	}
	prev_cpu_load[0] = total_cpu_load;

	// find average
	float avg = 0;
	for(i=0; i<FILTER_SAMPLES; i++){
		avg += prev_cpu_load[i];
	}
	avg /= FILTER_SAMPLES;
	data->cpu_load_10s = avg;

	return;
}

// concatenates directories
static void _concat_dir(char dir_array[][100], char *dir_prefix, char *dir_suffix, int32_t cpu_count)
{
	char cpu_val_str[2];
	for (int idx = 0; idx < cpu_count; idx++){
		sprintf(cpu_val_str, "%d", idx); // int to str
		strcpy(dir_array[idx], dir_prefix);
		strcat(dir_array[idx], cpu_val_str);
		strcat(dir_array[idx], dir_suffix);
	}
}

// concatenates directories for thermal paths
static void _concat_cpu_thermal_dir(char dir_array[][100], int *thermal_zones, int32_t cpu_count)
{
	char thermal_val_str[3];
	for (int idx = 0; idx < cpu_count; idx++){
		sprintf(thermal_val_str, "%d", thermal_zones[idx]); // int to str
		strcpy(dir_array[idx], SYSTEM_THERMAL_DIRECTORY);
		strcat(dir_array[idx], thermal_val_str);
		strcat(dir_array[idx], SYSTEM_CURRENT_TEMPERATURE);
	}
}

int main(int argc, char* argv[])
{
	// check for options
	if(_parse_opts(argc, argv)) return -1;

	printf("loading our config file\n");
	if(config_file_read()) return -1;
	config_file_print();

////////////////////////////////////////////////////////////////////////////////
// gracefully handle an existing instance of the process and associated PID file
////////////////////////////////////////////////////////////////////////////////

	// make sure another instance isn't running
	// if return value is -3 then a background process is running with
	// higher privaledges and we couldn't kill it, in which case we should
	// not continue or there may be hardware conflicts. If it returned -4
	// then there was an invalid argument that needs to be fixed.
	if(kill_existing_process(PROCESS_NAME, 2.0)<-2) return -1;

	// start signal handler so we can exit cleanly
	if(enable_signal_handler()==-1){
		fprintf(stderr,"ERROR: failed to start signal handler\n");
		return -1;
	}

////////////////////////////////////////////////////////////////////////////////
// do any setup and start threads HERE
////////////////////////////////////////////////////////////////////////////////

	// make PID file to indicate your project is running
	// due to the check made on the call to rc_kill_existing_process() above
	// we can be fairly confident there is no PID file already and we can
	// make our own safely.
	make_pid_file(PROCESS_NAME);

////////////////////////////////////////////////////////////////////////////////
// all threads started, wait for signal handler to stop it
////////////////////////////////////////////////////////////////////////////////
	_get_gpu_frequencies(); // initializes gpu frequency values

	int num_cpu = _get_cpu_count(); // Counts number of CPU's in system

	// thermal zones for cpu's are in different locations based on cpu count
	int cpu_thermal_zone_array_QRB5165[8] = {1,2,3,4,7,8,9,10};
	int cpu_thermal_zone_array_APQ8096[4] = {4, 6, 9, 11};
	char pop_mem_thermal_zone_QRB5165[] = "/sys/devices/virtual/thermal/thermal_zone28/temp";
	char pop_mem_thermal_zone_APQ8096[] = "/sys/devices/virtual/thermal/thermal_zone1/temp";
	
	float thermal_scaling_factor;
	int* thermal_zone_array;
	char* pop_mem_thermal_zone;

	if(num_cpu == 4){ // 4 cpu's
		chip = CHIP_APQ8096;
		thermal_zone_array = cpu_thermal_zone_array_APQ8096;
		pop_mem_thermal_zone = pop_mem_thermal_zone_APQ8096;
		thermal_scaling_factor = 0.1;
	}
	else if(num_cpu == 8){ // 8 cpu's
		chip = CHIP_QRB5165;
		thermal_zone_array = cpu_thermal_zone_array_QRB5165;
		pop_mem_thermal_zone = pop_mem_thermal_zone_QRB5165;
		thermal_scaling_factor = 0.001;
	}
	else{
		fprintf(stderr, "ERROR detected %d cpu, expected 4 or 8\n", num_cpu);
		return -1;
	}
	
	// creating file paths for different values
	char cpu_cur_freq_dir[8][100] = {""};
	_concat_dir(cpu_cur_freq_dir, SYSTEM_SPECIFIC_CPU_DIRECTORY_PREFIX, SYSTEM_CPU_SCALING_FREQUENCY, num_cpu);

	char cpu_max_freq_dir[8][100] = {""};
	_concat_dir(cpu_max_freq_dir, SYSTEM_SPECIFIC_CPU_DIRECTORY_PREFIX, SYSTEM_CPU_MAXIMUM_FREQUENCY, num_cpu);

	char cpu_weighted_freq_dir[8][100] = {""};
	_concat_dir(cpu_weighted_freq_dir, SYSTEM_SPECIFIC_CPU_DIRECTORY_PREFIX, SYSTEM_CPU_FREQUENCY_COUNTERS, num_cpu);

	_concat_dir(cpu_governor_dir, SYSTEM_SPECIFIC_CPU_DIRECTORY_PREFIX, SYSTEM_CPU_GOVERNOR_PATH, num_cpu);

	char cpu_thermal_zones[8][100] = {""};
	_concat_cpu_thermal_dir(cpu_thermal_zones, thermal_zone_array, num_cpu);

	float cpu_max_freq, cpu_weighted_freq;





	// create the server pipe
	pipe_info_t info = { \
		.name        = PIPE_NAME,\
		.location    = PIPE_LOCATION ,\
		.type        = "cpu_stats_t",\
		.server_name = PROCESS_NAME,\
		.size_bytes  = MODAL_PIPE_DEFAULT_PIPE_SIZE};

	strcpy(info.name, PIPE_NAME);
	strcpy(info.type, "cpu_stats_t");
	strcpy(info.server_name, PROCESS_NAME);
	info.size_bytes = CPU_STATS_RECOMMENDED_PIPE_SIZE;

	pipe_server_set_control_cb(SERVER_PIPE_CH, &_control_pipe_handler, NULL);
	pipe_server_set_connect_cb(SERVER_PIPE_CH, &_connect_handler, NULL);
	pipe_server_set_disconnect_cb(SERVER_PIPE_CH, &_disconnect_handler, NULL);
	pipe_server_set_available_control_commands(SERVER_PIPE_CH, CPU_MON_CONTROL_COMMANDS);
	if(pipe_server_create(SERVER_PIPE_CH, info, SERVER_FLAG_EN_CONTROL_PIPE)) return -1;

	// open client pipe for autopilot armed status if using auto standby
	if(en_auto_standby || en_debug){
		_connect_to_autopilot();
	}

	// set fan speed if not running auto fan
	if(fan_mode != FAN_MODE_AUTO) _set_fan(fan_mode);


	main_running = 1; // this is an extern variable in start_stop.c
	printf("Init complete, entering main loop\n");
	while(main_running){

		// initiailize data struct
		cpu_stats_t data;
		memset(&data, 0, sizeof(cpu_stats_t));
		data.magic_number = CPU_MON_MAGIC_NUMBER;
		data.num_cpu = num_cpu;

		// check if we have a client
		int has_client = pipe_server_get_num_clients(SERVER_PIPE_CH) > 0;

		// run auto standby mode
		if(en_auto_standby){
			if(is_autopilot_armed) current_standby_state = 0;
			else current_standby_state = 1;
		}

		// check current frequency scaling modes as they may have changed
		_get_gpu_mode();
		_get_cpu_mode();

		// set cpu scalers based on standby mode if needed
		if(current_standby_state == STANDBY_STATE_OFF){
			if(cpu_mode!=normal_cpu_mode){
				_set_cpu_mode(normal_cpu_mode);
			}
		}
		else if(current_standby_state == STANDBY_STATE_ON){
			if(cpu_mode!=standby_cpu_mode){
				_set_cpu_mode(standby_cpu_mode);
			}
		}


		// check temperature if we have a client or auto fan is enabled
		if(has_client || fan_mode == FAN_MODE_AUTO){
			float sum_t = 0.0;
			for(int i=0; i<num_cpu; i++){
				data.cpu_t[i] = _get_x_current_temp(cpu_thermal_zones[i], thermal_scaling_factor);
				sum_t += data.cpu_t[i];
			}
			data.cpu_t_max = sum_t/num_cpu;
			current_cpu_temp = data.cpu_t_max;
			// do auto fan if enabled
			if(fan_mode == FAN_MODE_AUTO){
				_set_fan(FAN_MODE_AUTO);
			}
		}

		// Only publish data if we have clients
		if(has_client || en_debug){

			// Grab CPU info
			for(int i=0; i<num_cpu; i++){
				data.cpu_freq[i]	= _get_cpu_current_frequency(cpu_cur_freq_dir[i]);

				// core is off, load is 0
				if(data.cpu_freq[i]==0.0f){
					data.cpu_load[i] = 0.0;
					continue;
				}
				// otherwise calc properly
				cpu_max_freq		= _get_cpu_max_freq(cpu_max_freq_dir[i]);
				cpu_weighted_freq	= _get_cpu_weighted_frequency(i, cpu_weighted_freq_dir[i]);
				data.cpu_load[i]	= _get_cpu_load(i, cpu_max_freq, cpu_weighted_freq);
			}
			_get_total_cpu_load(&data);

			// Grab GPU info
			data.gpu_t = _get_x_current_temp(SYSTEM_GPU_CURRENT_TEMPERATURE, thermal_scaling_factor);
			_get_gpu_load(&data);

			// Grab memory info
			data.mem_t = _get_x_current_temp(pop_mem_thermal_zone, thermal_scaling_factor);
			_get_mem_used(&data);

			// set cpu mode flags
			if(cpu_mode == CPU_MODE_AUTO){
				data.flags |= CPU_STATS_FLAG_CPU_MODE_AUTO;
			}
			else if(cpu_mode == CPU_MODE_PERF){
				data.flags |= CPU_STATS_FLAG_CPU_MODE_PERF;
			}
			else if(cpu_mode == CPU_MODE_POWERSAVE){
				data.flags |= CPU_STATS_FLAG_CPU_MODE_POWERSAVE;
			}
			else if(cpu_mode == CPU_MODE_CONSERVATIVE){
				data.flags |= CPU_STATS_FLAG_CPU_MODE_CONSERVATIVE;
			}

			// set GPU mode flags
			if(gpu_mode == GPU_MODE_AUTO){
				data.flags |= CPU_STATS_FLAG_GPU_MODE_AUTO;
			}
			else if(gpu_mode == GPU_MODE_PERF){
				data.flags |= CPU_STATS_FLAG_GPU_MODE_PERF;
			}

			// set overload flags
			if(data.total_cpu_load > CPU_STATS_CPU_OVERLOAD_THRESHOLD){
				data.flags |= CPU_STATS_FLAG_CPU_OVERLOAD;
			}
			if(data.cpu_t_max > CPU_STATS_CPU_OVERHEAT_THRESHOLD){
				data.flags |= CPU_STATS_FLAG_CPU_OVERHEAT;
			}

			// set standby flags
			if(current_standby_state == STANDBY_STATE_ON){
				data.flags |= CPU_STATS_FLAG_STANDBY_ACTIVE;
			}

			// write data out
			pipe_server_write(SERVER_PIPE_CH, (char*)&data, sizeof(cpu_stats_t));
		}
		if(led_flashing) _update_led();

		usleep(1000000/SAMPLE_RATE_HZ);
	}

////////////////////////////////////////////////////////////////////////////////
// Stop all the threads and do cleanup HERE
////////////////////////////////////////////////////////////////////////////////

	printf("Starting shutdown sequence\n");
	pipe_server_close_all();
	pipe_client_close_all();
	remove_pid_file(PROCESS_NAME);
	printf("exiting cleanly\n");
	return 0;
}
