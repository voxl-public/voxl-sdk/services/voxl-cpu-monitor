/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef CONFIG_FILE_H
#define CONFIG_FILE_H


#include <modal_json.h>

#define CONF_FILE "/etc/modalai/voxl-cpu-monitor.conf"
#define MAX_PAIRS	2

#define CONFIG_FILE_HEADER "\
/**\n\
 * This file contains configuration parameters for voxl-cpu-monitor.\n\
 *\n\
 *\n\
 * en_auto_standby:   enable cpu-monitor to put CPU in standby mode automatically\n\
 *                      when Autopilot is disarmed\n\
 *\n\
 * normal_cpu_mode:   cpu governor to use normally, default: auto\n\
 *                      options are: auto, performance, powersave, conservative\n\
 *\n\
 * standby_cpu_mode:  cpu governor to use when in standby mode, default: powersave\n\
 *                    options are: auto, performance, powersave, conservative\n\
 *\n\
 * fan_mode:          fan speed governor to use, default: auto\n\
 *                    options are: auto, off, max, slow\n\
 *\n\
 */\n"


#define CPU_MODE_STRINGS {"auto","performance","powersave","conservative"}
#define FAN_MODE_STRINGS {"auto","off","max","slow"}

static int en_auto_standby;
static int normal_cpu_mode;
static int standby_cpu_mode;
static int fan_mode;

/**
 * load the config file and populate the above extern variables
 *
 * @return     0 on success, -1 on failure
 */
static void config_file_print(void)
{
	const char* cpu_mode_strings[] = CPU_MODE_STRINGS;
	const char* fan_mode_strings[] = FAN_MODE_STRINGS;
	printf("=================================================================\n");
	printf("en_auto_standby:  %d\n", en_auto_standby);
	printf("normal_cpu_mode:  %s\n", cpu_mode_strings[normal_cpu_mode]);
	printf("standby_cpu_mode: %s\n", cpu_mode_strings[standby_cpu_mode]);
	printf("fan_mode:         %s\n", fan_mode_strings[fan_mode]);
	printf("=================================================================\n");
	return;
}




/**
 * @brief      prints the current configuration values to the screen
 *
 *             this includes all of the extern variables listed above. If this
 *             is called before config_file_load then it will print the default
 *             values.
 */
static int config_file_read(void)
{
	int ret = json_make_empty_file_with_header_if_missing(CONF_FILE, CONFIG_FILE_HEADER);
	if(ret < 0) return -1;
	else if(ret>0) fprintf(stderr, "Creating new config file: %s\n", CONF_FILE);

	cJSON* parent = json_read_file(CONF_FILE);
	if(parent==NULL) return -1;



	json_fetch_int_with_default(parent, "en_auto_standby", &en_auto_standby, 0);


	const char* cpu_mode_strings[] = CPU_MODE_STRINGS;
	const int n_cpu_modes = sizeof(cpu_mode_strings)/sizeof(cpu_mode_strings[0]);
	json_fetch_enum_with_default(   parent, "normal_cpu_mode",  (int*)&normal_cpu_mode, cpu_mode_strings, n_cpu_modes, 0); // auto
	json_fetch_enum_with_default(   parent, "standby_cpu_mode", (int*)&standby_cpu_mode, cpu_mode_strings, n_cpu_modes, 2); // powersave

	const char* fan_mode_strings[] = FAN_MODE_STRINGS;
	const int n_fan_modes = sizeof(fan_mode_strings)/sizeof(fan_mode_strings[0]);
	json_fetch_enum_with_default(   parent, "fan_mode",  (int*)&fan_mode, fan_mode_strings, n_fan_modes, 0); // auto


	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse config file %s\n", CONF_FILE);
		cJSON_Delete(parent);
		return -1;
	}

	// write modified data to disk if neccessary
	if(json_get_modified_flag()){
		json_write_to_file_with_header(CONF_FILE, parent, CONFIG_FILE_HEADER);
	}
	cJSON_Delete(parent);



	return 0;
}




#endif // end CONFIG_FILE_H
